public class test {
    public static void main(String[] args) {
        System.out.println("Das ist ein Beispielsatz. Ein Beispielsatz ist das.\n");
        System.out.println("Das ist ein \"Beispielsatz\"\n" + "Ein Beispielsatz ist das.\n");
        System.out.println("      *");
        System.out.println("     ***");
        System.out.println("    *****");
        System.out.println("   *******");
        System.out.println("  *********");
        System.out.println(" ***********");
        System.out.println("*************");
        System.out.println("     ***");
        System.out.println("     ***\n");

        double d1 = 22.4234234;
        double d2 = 111.2222;
        double d3 = 4.0;
        double d4 = 1000000.551;
        double d5 = 97.34;
        System.out.printf("%.2f \n" , d1);
        System.out.printf("%.2f \n" , d2);
        System.out.printf("%.2f \n" , d3);
        System.out.printf("%.2f \n" , d4);
        System.out.printf("%.2f \n" , d5);

        System.out.println("Test");
    }
}
